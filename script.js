// Select the h1 element and store it in a variable named heading.
let headings = document.querySelector('h1')

// Check the typeof heading and log it.
console.log(typeof headings)

// Change the color of heading to black.
headings.style.color = "black"


// Select all the hr elements and store it in a variable named allHrs using querySelectorAll
let allHrs = document.querySelectorAll('hr')

/* 
Convert the NodeList returned by querySelectorAll to Array using Array.from() or spread operator and store it in allHrsArray
Array.from converts an array kind of data into array so we can use methods like map, reduce
HINT:
*/
let allHrsArray = Array.from(allHrs)
console.log(allHrsArray.map(hr => { return hr
}))


// Set the border of the all the hr elements to "1px solid tomato"
let hrElement = document.querySelectorAll('hr')
for(let i=0 ; i < hrElement.length ; i++){
  hrElement[i].style.border  = "1px solid tomato"
}


// Change the background of all the hr to "antiquewhite" using for of loop.
let toChangeBackgroundHr = document.querySelectorAll('hr')
for(let i = 0; i < hrElement; i++){
  toChangeBackgroundHr[i].style.backgraound = "antiquewhite"
}

// Change the 'border-radius' of all the hr to "5px" using array.
for(let i=0 ; i < hrElement.length ; i++){
  hrElement[i].style.borderRedius  = "5px"
}

// Change the alignment of the heading(h1) to center.
headings.style.textAlign = "center"


// Change the font size of the heading to 3rem.
headings.style.fontSize = "3rem"


// Change the border of hr with class 'image' to `2px solid purple`.
let borderImage = document.querySelectorAll('.image')
for(let i =0 ; i < borderImage.length ; i++){
  borderImage[i].style.border = "2px solid purple"

}
// Hide the box number 17 (last box).
let lastBox = document.querySelector(".seventeen")
lastBox.style.display = "none"

// Change the border of all the hr element from solid to dashed type
for (let i=0; i < allHrs.length ; i++){
  allHrs[i].style.border = "dashed"
}

// Create a pragraph element and store it in variable named 'para' using `createElement`
let para = document.createElement('p')

// Change the inner text of para to "querySelector returns an element you can maupulate but querySelectorAll returns the collection of elements in array kind of structure."
para.innerText = "querySelector returns an element you can maupulate but querySelectorAll returns the collection of elements in array kind of structure."

// Remove all the elements from box 1
const firstBoxRemove = document.querySelector(".one")
const eleFromOne = firstBoxRemove.querySelectorAll("hr")
for(let i =0; i < eleFromOne.length ; i++){
  eleFromOne[i].remove()
}
// Replace all the elements inside box 1 with the para (you created above)
let div1 = document.querySelector(".one")
div1.append(para)

/* Walking the DOM
Do the following after selecting box 16 and storing in variable named box16
  - Access the parentNode
  - Access the childNodes
  - Access previousSibling
  - Access nextSibling
  - Access firstChild
  - Access lastChild
  - Access previousElementSibling
  - Access nextElementSibling
  - Access firstElementChild
  - Access lastElementChild
  - Focus on the difference between element and node
*/
let boxSixteen = document.querySelector(".sixteen")
console.log(boxSixteen.parentNode)
console.log(boxSixteen.childNodes)
console.log(boxSixteen.previousElementSibling)
console.log(boxSixteen.nextSibling)
console.log(boxSixteen.firstChild)
console.log(boxSixteen.lastChild)
console.log(boxSixteen.previousElementSibling)
console.log(boxSixteen.nextElementSibling)
console.log(boxSixteen.lastElementChild)
// Select box 2 and append a new paragraph element with content "Append inserts as last child" just after hr element.
document.querySelector(".two").append('Append inserts as last child')


// Select box 3 and prepend a new paragraph element with content "Prepend inserts as first child" just before hr element.
document.querySelector('.three').prepend("Prepend inserts as first child")

// Change the border of box 4 to '1px solid black'
let boxFour =  document.querySelector(".four")
boxFour.style.border = '1px solid black'

// Change the border radius of box 5 to 10px.
let boxFive = document.querySelector(".five")
// boxFive.style.borderRedius = "10px"


// Change the text color of box 6 to black.
let boxSix = document.querySelector(".six")
boxSix.style.color = "black"

// Change the font size of the para inside box 1 to 0.8rem.
para.style.fontSize = "0.8rem"

// Change the background of all the alternate boxes (1, 3, 5, ....) to aliceblue

let oddBoxes = document.querySelectorAll(".box:nth-child(odd)")
for(let i = 0; i < oddBoxes.length ; i++){
  oddBoxes[i].style.background = "aliceblue"
}

// add a class named "awesome-box" to the box 6 using classList property of DOM element.
let asBox = document.querySelector(".six")
asBox.classList.add("awesome-box") 

// Using the toggle classList property toggle the class `awesome-box` from box 2



// Using the remove classList proeprty remove the class `awesome-box` from box 4
  let box4 = document.querySelector(".four")
  box4.classList.remove("awesome-box") 

// Change the background of the body to bisque
document.querySelector("body").style.background = "bisque"

// Create a button and store it in a variable named 'btn'
let btn = document.createElement('button')

// textContent of the button should be 'Click Me'
btn.textContent ='Click Me'

// Change the background of the btn to 'oldlace'
btn.style.background = 'oldlace'

// Change the font size of the btn to 1rem
btn.style.fontSize = '1rem'

// Change the border of the btn to '1px solid black'
btn.style.border = "1px solid black"

// Add the padding of '0.5rem 1rem' to btn
btn.style.padding = "0.5rem 1rem"

// Append the btn in box number 9
let boxNine = document.querySelector('.nine')
boxNine.append(btn)

// Create a img element with src value `https://images.unsplash.com/photo-1592500595497-d1f52a40b207?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=80` and store in a variable named imgElm
var url = 'https://images.unsplash.com/photo-1621839673705-6617adf9e890?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1332&q=80'; 
let imgElm = document.createElement('img')
imgElm.src = url;

// Select the box 7 using class seven
let boxSeven = document.querySelector('.seven')
console.log(boxSeven)

// Remove all the elements form box seven
boxSeven.innerHTML=""

// Append the imgElm to the box no 7
boxSeven.appendChild(imgElm)

// Change the width and height of the image to `100%`
imgElm.style. width = "100%";
imgElm.style. height = '100%';

// Select the box 5 using class five
let box5 = document.querySelector(".five");

// Create an input element
let inputElement = document.createElement('input')

// Change the placeholder property of the input element to "Enter you email!"
inputElement.placeholder = "Enter your email!"


// Append the input element to the box 5 you selected above
box5.append(inputElement)

// Create two anchor (a) element with  the text of `AltCampus` and `Google`
let anchor1 = document.createElement('a')
anchor1.innerText ='AltCampus'
let anchor2 = document.createElement('a')
anchor2.innerText ='Google'

// Change the href property of the anchor elements to `https://www.mountblue.io/` and `https://google.com`
anchor1.href = 'https://www.mountblue.io/'
anchor2.href = 'https://google.com'

// Append both the elements to box 5 you selected above.

box5.append(anchor1,anchor2)
// box5.append(anchor2)

